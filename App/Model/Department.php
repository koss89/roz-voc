<?php

namespace App\Model;

class Department
{
    private int $id;
    private User $lead;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getLead(): User
    {
        return $this->lead;
    }

    /**
     * @param User $lead
     */
    public function setLead(User $lead): void
    {
        $this->lead = $lead;
    }



}