<?php


namespace App\Serv\Impl;


use App\Exceptions\PermissionException;
use App\Exceptions\ValidationException;
use App\Model\Department;
use App\Model\User;
use App\Model\Vocation;
use App\Repo\IVocationRepo;
use App\Serv\IVocationHistoryServ;
use App\Serv\IVocationServ;

class VocationServ extends AbstractServ implements IVocationServ
{
    private IVocationRepo $vocationRepo;
    private IVocationHistoryServ $vocationHistoryServ;

    /**
     * VocationServ constructor.
     */
    public function __construct(
        IVocationRepo $vocationRepo,
        IVocationHistoryServ $vocationHistoryServ
    )
    {
        $this->vocationRepo = $vocationRepo;
        $this->vocationHistoryServ = $vocationHistoryServ;
    }

    private function validate(Vocation $vocation)
    {
        $start = $vocation->getStart();
        $end = $vocation->getEnd();
        if($this->vocationRepo->hasVocationInPeriod($start, $end)) {
            throw new ValidationException('already has vocation');
        }
        $now = time();
        if($now>$start->getTimestamp() || $start->getTimestamp()>$end->getTimestamp()) {
            throw new ValidationException('invalid date');
        }
    }

    private function canResolution(int $user_id, Department $department): bool
    {
        if($department->getLead()->getId()!==$user_id) {
            throw new PermissionException('access deny');
        }
        return true;
    }

    public function save(Vocation $vocation): Vocation
    {
        $currentUser = $this->getCurrentUser();

        $this->validate($vocation);
        if($vocation->getId()) {
            //get Model or throw Exception
            $oldVocation = $this->vocationRepo->getByIdAndUser($vocation->getId(), $currentUser->getId());
            if($oldVocation->getStatus()!='CREATED') {
                throw new PermissionException('access deny');
            }
        } else {
            $vocation->setUser($currentUser);
            $vocation->setStatus('CREATED');
        }
        $this->vocationRepo->save($vocation);
        $this->vocationHistoryServ->save($vocation);

        return $vocation;
    }

    public function resolution(Vocation $vocation, User $user, string $resolution)
    {
        $this->canResolution($user->getId(), $vocation->getUser()->getDepartment());
        $vocation->setStatus($resolution);
        $this->vocationRepo->save($vocation);
        $this->vocationHistoryServ->save($vocation, $resolution, $user);
    }
}