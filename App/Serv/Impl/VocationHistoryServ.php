<?php


namespace App\Serv\Impl;


use App\Model\User;
use App\Model\Vocation;
use App\Model\VocationHistory;
use App\Repo\IVocationHistoryRepo;
use App\Serv\IVocationHistoryServ;

class VocationHistoryServ extends AbstractServ implements IVocationHistoryServ
{
    private IVocationHistoryRepo $vocationHistoryRepo;

    public function __construct(IVocationHistoryRepo $vocationHistoryRepo)
    {
        $this->vocationHistoryRepo = $vocationHistoryRepo;
    }

    public function save(Vocation $vocation, string $status = null, User $user = null): VocationHistory
    {
        $history = new VocationHistory();

        if(!$user) {
            $user = $this->getCurrentUser();
        }
        $history->setVocation($vocation);
        $history->setUser($user);
        $history->setCreated(new \DateTime());
        if(!$status) {
            $status = $vocation->getStatus();
        }
        $history->setStatus($status);

        return $this->vocationHistoryRepo->save($history);
    }
}