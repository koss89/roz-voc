<?php

namespace App\Serv;

use App\Model\User;
use App\Model\Vocation;
use App\Model\VocationHistory;

interface IVocationHistoryServ
{
    public function save(Vocation $vocation, string $status = null, User $user = null): VocationHistory;
}