<?php

namespace App\Serv;

use App\Model\User;
use App\Model\Vocation;

interface IVocationServ
{
    public function save(Vocation $vocation): Vocation;
    public function resolution(Vocation $vocation, User $user, string $resolution);
}