<?php

namespace App\Repo;

use App\Model\VocationHistory;

interface IVocationHistoryRepo
{
    public function save(VocationHistory $vocation): VocationHistory;
}