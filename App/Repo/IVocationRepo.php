<?php

namespace App\Repo;

use App\Model\Vocation;

interface IVocationRepo
{
    public function getByIdAndUser(int $vocation_id, int $user_id): Vocation;
    public function save(Vocation $vocation): Vocation;
    public function hasVocationInPeriod(\DateTime $start, \DateTime $end): bool;

}